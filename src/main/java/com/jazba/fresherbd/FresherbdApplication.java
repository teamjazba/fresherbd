package com.jazba.fresherbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FresherbdApplication {

    public static void main(String[] args) {
        SpringApplication.run(FresherbdApplication.class, args);
    }

}
