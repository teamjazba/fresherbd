package com.jazba.fresherbd.controller;
import com.jazba.fresherbd.configuration.Utils;
import com.jazba.fresherbd.fresher.FresherDto;
import com.jazba.fresherbd.fresher.FresherService;
import com.jazba.fresherbd.user.User;
import com.jazba.fresherbd.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    FresherService fresherService;

    @GetMapping("/")
    public String home(Model model){
    /*    User user=userService.getUserByUsername(Utils.getLoggedUserName());
        model.addAttribute("user",user);
        FresherDto fresher=fresherService.findFresherByUserId(user.getUserId());
        System.err.println(fresher.toString());
        model.addAttribute("fresher",fresher);*/
        return "index";
    }

}
