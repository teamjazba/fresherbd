package com.jazba.fresherbd.fresher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
public class FresherController {
    
    @Autowired
    private FresherService fresherService;

    @GetMapping("/fresherCreate")
    public String fresherCreate() {
        return "fresher/fresherCreate";
    }

    @ResponseBody
    @PostMapping("/fresherCreate")
    public FresherDto fresherCreate(@RequestBody FresherDto fresherDto) {
        FresherDto fresherInfo=fresherService.save(fresherDto);
        return fresherInfo;
    }

    @GetMapping("/fresherList")
    public String fresherList() {
        List<FresherDto> fresherList = fresherService.list();
        return "fresher/fresherList";
    }

    @GetMapping("/fresherEdit")
    public String fresherEdit(@RequestParam(value = "fresherId", required = true) Integer id, Model model) {
        FresherDto fresher = fresherService.findFresherById(id);
        model.addAttribute("fresher", fresher);
        return "fresher/fresherCreate";
    }

    @GetMapping("/fresherDelete")
    public void deleteFresher(@RequestParam(value = "id", required = true) Integer id) {
        fresherService.deleteFresherById(id);
    }


    @GetMapping("/edit")
    public String fresherEdit() {
        return "fresher/fresher";
    }

}
