package com.jazba.fresherbd.fresher;
import com.jazba.fresherbd.user.User;
import lombok.Data;

@Data
public class FresherDto {
    private Integer fresherId;
    private String name;
    private String contact;
    private String gender;
    private String email;
    private String address;
    private String birthDate;
    private String university;
    private String subject;
    private String graduationYear;
    private String cgpa;
    private String researchTopic;
    private String degreeHeld;
    private String school;
    private String schoolPassingYear;
    private String schoolGPA;
    private String college;
    private String collegePassingYear;
    private String collegeGPA;
    private String skills;
    private String expertiseArea;
    private String technicalAbility;
    private String extraCurricular;
    private User user;
}
