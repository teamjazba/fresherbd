package com.jazba.fresherbd.fresher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FresherRepository extends JpaRepository<FresherEntity, Integer> {
/*
    @Query(value = "SELECT *FROM fresher_entity WHERE fresher_entity.user_user_id=?1", nativeQuery = true)
    FresherEntity getFresherEntityByUserId(@Param("id") int id);*/
}