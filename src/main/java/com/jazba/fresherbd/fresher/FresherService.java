package com.jazba.fresherbd.fresher;

import com.jazba.fresherbd.configuration.Utils;
import com.jazba.fresherbd.user.User;
import com.jazba.fresherbd.user.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class FresherService {

    @Autowired
    private FresherRepository fresherRepository;

    @Autowired
    private UserService userService;

    public FresherDto save(FresherDto fresherDto){
        FresherEntity fresherEntity = new FresherEntity();
       /* User user=userService.getUserByUsername(Utils.getLoggedUserName());
        fresherDto.setUser(user);*/
        BeanUtils.copyProperties(fresherDto,fresherEntity);
        try{
            FresherEntity savedEntity=fresherRepository.save(fresherEntity);
            FresherDto savedDto=new FresherDto();
            BeanUtils.copyProperties(savedEntity,savedDto);
            return savedDto;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<FresherDto> list(){
        List<FresherEntity> fresherEntityList=fresherRepository.findAll();
        List<FresherDto> fresherDtoList=new ArrayList<FresherDto>();
        BeanUtils.copyProperties(fresherEntityList,fresherDtoList);
        return fresherDtoList;
    }

    public FresherDto findFresherById(Integer id){
        FresherEntity fresherEntity=new FresherEntity();
        FresherDto fresherDto=new FresherDto();
        fresherEntity=fresherRepository.findOne(id);
        BeanUtils.copyProperties(fresherEntity,fresherDto);
        return fresherDto;
    }



    public void deleteFresherById(Integer id){
      fresherRepository.delete(id);
    }

   /* public FresherDto findFresherByUserId(Integer id){
        FresherEntity fresherEntity=fresherRepository.getFresherEntityByUserId(id);
        FresherDto fresherDto=new FresherDto();
        BeanUtils.copyProperties(fresherEntity,fresherDto);
        return fresherDto;
    }*/

}
