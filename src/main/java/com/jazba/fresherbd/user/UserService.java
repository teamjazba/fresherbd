package com.jazba.fresherbd.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public User save(User user){
        user.setEnabled(true);
        if(user.getType().equals("Fresher")){
            user.setRole("ROLE_FRESHER");
        }
        else{
            user.setRole("ROLE_RECRUITER");
        }
        return repository.save(user);
    }

    public List<User> userList() {
        return repository.findAll();
    }

    public User findById(Integer Id){
        return repository.getOne(Id);
    }

    public void deleteById(Integer Id){
         repository.delete(Id);
    }

    public User getUserByUsername(String loggedUserName) {
        return repository.findUserByUsername(loggedUserName);
    }
}
