function addFresherInfo() {
    var formValues = $('#fresherForm').serializeJSON();
    var jsonFresherInfoFormData = JSON.stringify(formValues);

    console.log(jsonFresherInfoFormData);
        $.ajax({
            url: '/fresherCreate',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: jsonFresherInfoFormData,
            beforeSend: function (xhr) {
                xhr.withCredentials = true;
            },
            success: function (fresherInfo) {
                $('#fresher-modal').modal('hide');
                    var str="<tr>" +
                        "<td style=\"display:none;\">" + fresherInfo.fresherId +"</td>"+
                        "<td>"+ fresherInfo.name +"</td>"+
                        "<td>"+ fresherInfo.contact +"</td>"+
                        "<td>"+ fresherInfo.gender +"</td>"+
                        "<td>"+ fresherInfo.email +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.address +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.birthDate +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.university +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.subject +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.degreeHeld +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.graduationYear +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.cgpa +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.researchTopic +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.college +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.collegePassingYear +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.collegeGPA +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.school +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.schoolPassingYear +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.schoolGPA +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.skills +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.expertiseArea +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.technicalAbility +"</td>"+
                        "<td style=\"display:none;\">" + fresherInfo.extraCurricular +"</td>"+
                        "<td><a class=\"btn btn-warning\" onclick=\"editFresher(this)\"><i class=\"fa fa-warning\"></i>Edit</a>\n" +
                        "<a class=\"btn btn-danger\" onclick=\"deleteFresher(this)\"> <i class=\"fa fa-trash\"></i>Delete</a></td>" +
                        "</tr>";
                    $('#fresherTable_tbody').append(str);
            },
            error: function (err) {
                console.log(err);
            }
        });
}

$('#fresherSubmit').click(function(event) {
    event.preventDefault();
    addFresherInfo();
});


function editFresher(node){
   var row=node.parentNode.parentNode;
    $("#fresherId").val(row.cells[0].innerHTML);
    $("#name").val(row.cells[1].innerHTML);
    $("#contact").val(row.cells[2].innerHTML);
    $("#gender").val(row.cells[3].innerHTML);
    $("#email").val(row.cells[4].innerHTML);
    $("#address").val(row.cells[5].innerHTML);
    $("#birthDate").val(row.cells[6].innerHTML);
    $("#university").val(row.cells[7].innerHTML);
    $("#subject").val(row.cells[8].innerHTML);
    $("#degreeHeld").val(row.cells[9].innerHTML);
    $("#graduationYear").val(row.cells[10].innerHTML);
    $("#cgpa").val(row.cells[11].innerHTML);
    $("#researchTopic").val(row.cells[12].innerHTML);
    $("#college").val(row.cells[13].innerHTML);
    $("#collegePassingYear").val(row.cells[14].innerHTML);
    $("#collegeGPA").val(row.cells[15].innerHTML);
    $("#school").val(row.cells[16].innerHTML);
    $("#schoolPassingYear").val(row.cells[17].innerHTML);
    $("#schoolGPA").val(row.cells[18].innerHTML);
    $("#skills").val(row.cells[19].innerHTML);
    $("#expertiseArea").val(row.cells[20].innerHTML);
    $("#technicalAbility").val(row.cells[21].innerHTML);
    $("#extraCurricular").val(row.cells[22].innerHTML);
    $('#fresher-modal').modal('show');
}

function deleteFresher(node){
    var fresherId=node.parentNode.parentNode.cells[0].innerHTML;
    alert("Delete Button Clicked!"+fresherId);
    $.ajax({
        url: "/fresherDelete?fresherId="+fresherId,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            alert("Successfully Removed!");
        },
        error: function (data) {
            //alert("Error!");
        }
    });
    $(node).closest("tr").remove();
}